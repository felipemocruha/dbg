package dbg

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"reflect"
	"runtime"
	"strings"
	"unsafe"
)

var dbg_GLOBAL_STATE context.Context

func init() {
	dbg_GLOBAL_STATE = context.Background()
}

// Ctx prints the current stored state.
func Ctx() {
	fmt.Printf("## Beginning of Current Context:\n")
	ctxInner(dbg_GLOBAL_STATE)
	fmt.Printf("## End of Current Context\n")
}

// Key takes a key-value pair and stores it in current state
func Key(key string, val interface{}) {
	ctx := dbg_GLOBAL_STATE
	dbg_GLOBAL_STATE = context.WithValue(ctx, key, val)
}

// Msg takes a variadic number of parameters.
// If none is provided, it'll just tell you where it passed.
// If any value is provided, it'll be printed to stdout.
func Msg(val ...interface{}) {
	if len(val) == 0 {
		fmt.Printf("%v => Hey!\n\n", traceCaller())
	} else {
		fmt.Printf("%v => %+v\n\n", traceCaller(), val)
	}
}

// Break adds a breakpoint in some position of the code.
// If `true` is passed as argument, it'll print current state.
func Break(showCtx ...bool) {
	fmt.Printf("# BREAKPOINT: %v\n", traceCaller())

	if len(showCtx) != 0 && showCtx[0] {
		Ctx()
	}

	fmt.Printf("<< Press Enter⏎ to continue. >>\n\n")
	stdin := bufio.NewReader(os.Stdin)

	for {
		_, err := fmt.Fscanln(stdin)
		if err == nil {
			break
		}
	}
}

type callerTrace struct {
	File     string
	Line     int
	Function string
}

func (t callerTrace) Format() string {
	return fmt.Sprintf("%v -> %v:%v", t.Function, t.File, t.Line)
}

func traceCaller() string {
	pc := make([]uintptr, 10)
	n := runtime.Callers(3, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	file := strings.Split(frame.File, "/")
	function := strings.Split(frame.Function, "/")

	return callerTrace{
		strings.Join(file[len(file)-2:], "/"),
		frame.Line,
		function[len(function)-1],
	}.Format()
}

func ctxInner(ctx interface{}) {
	keys, values := reflect.TypeOf(ctx).Elem(), reflect.ValueOf(ctx).Elem()

	if keys.Kind() == reflect.Struct {
		for i := 0; i < values.NumField(); i++ {
			val := values.Field(i)
			val = reflect.NewAt(val.Type(), unsafe.Pointer(val.UnsafeAddr())).Elem()
			field := keys.Field(i)

			if field.Name == "Context" {
				ctxInner(val.Interface())
			} else {
				if i == 1 {
					fmt.Printf("\t• %v: ", val.Interface())
				}

				if i == 2 {
					fmt.Printf("\t%+v \n", val.Interface())
				}
			}
		}
	}
}
