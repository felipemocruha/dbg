package dbg

import (
	"fmt"
)

func ExampleUsage() {
	// When you import dbg a global state is initialized.
	// That's where we can save key-value pairs of interesting variables
	// at runtime.

	// We can simply print a message to tell where our code passed.
	Msg()

	// We can also pass parameters as we like.
	// A Msg() call will not store values in global context, it just prints them.
	Msg("Here", "We", "Go!", 1, struct{}{})

	// To pass values between functions without hydrating
	// all the way down the call stack, add a key value pair
	Key("token", "1dd177c6-8d14-4131-aacb-b770051beef1")
	Key("object", map[string]int{"a": 1})

	// To show stored values, simply call
	Ctx()

	// It's possible to add simple breakpoints too.
	// This is useful when we need to understand or prepare
	// for something that is too fast.
	fmt.Println("We print this first.")
	Break()
	fmt.Println("Then we print this one...")

	// Also we can print the current ctx in breakpoints with
	Break(true)
}
