# dbg

dbg is a very simple debug library, it's just some boilerplate with zero dependencies to make life easier on programmatic bug hunting.

## Usage

```go
package main

import (
    "fmt"
    . "gitlab.com/felipemocruha/dbg"
)

func ExampleUsage() {
    // When you import dbg a global state is initialized.
    // That's where we can save key-value pairs of interesting variables
    // at runtime.

    // We can simply print a message to tell where our code passed.
    Msg()
    // ---
     // dbg.ExampleUsage -> dbg/dbg_test.go:13 => Hey!
    
    // We can also pass parameters as we like.
    // A Msg() call will not store values in global context, it just prints them.
    Msg("Here", "We", "Go!", 1, struct{}{})
    // ---
    // dbg.ExampleUsage -> dbg/dbg_test.go:17 => [Here We Go! 1 {}]
    
    // To pass values between functions without hydrating
    // all the way down the call stack, add a key value pair
    Key("token", "1dd177c6-8d14-4131-aacb-b770051beef1")
    Key("object", map[string]int{"a": 1})

    // To show stored values, simply call
    Ctx()
    // ---
    // ## Beginning of Current Context:
    //     • token:     1dd177c6-8d14-4131-aacb-b770051beef1
    //     • object:     map[a:1]
    // ## End of Current Context

    // It's possible to add simple breakpoints too.
    // This is useful when we need to understand or prepare
    // for something that is too fast.
    fmt.Println("We print this first.")
    // ---
    // We print this first.

    Break()
    // ---
    // # BREAKPOINT: dbg.ExampleUsage -> dbg/dbg_test.go:31
    // << Press Enter⏎ to continue. >>

    fmt.Println("Then we print this one...")
    // ---
    // Then we print this one...

    // Also, we can print the current ctx in breakpoints with
    Break(true)
    // ---
    // # BREAKPOINT: dbg.ExampleUsage -> dbg/dbg_test.go:35
    // ## Beginning of Current Context:
    //     • token:     1dd177c6-8d14-4131-aacb-b770051beef1
    //     • object:     map[a:1]
    // ## End of Current Context
    // << Press Enter⏎ to continue. >>
}
```

